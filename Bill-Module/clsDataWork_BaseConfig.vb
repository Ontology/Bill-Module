﻿Imports OntologyAppDBConnector
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Public Class clsDataWork_BaseConfig
    Private objLocalConfig As clsLocalConfig

    Private objDBLevel_Gross_Std As OntologyModDBConnector
    Private objDBLevel_Currencies As OntologyModDBConnector
    Private objDBLevel_Currencies_Combo As OntologyModDBConnector
    Private objDBLevel_Languages As OntologyModDBConnector
    Private objDBLevel_Partners As OntologyModDBConnector
    Private objDBLevel_TaxRate As OntologyModDBConnector
    Private objDBLevel_TaxRate_Combo As OntologyModDBConnector
    Private objDBLevel_Percent As OntologyModDBConnector
    Private objDBLevel_Unit As OntologyModDBConnector
    Private objDBLevel_Unit_Combo As OntologyModDBConnector
    Private objDBLevel_Unit_Amount_Standard As OntologyModDBConnector
    Private objDBLevel_SearchTemplates As OntologyModDBConnector

    Public ReadOnly Property OL_SearchTemplates As List(Of clsObjectRel)
        Get
            Return objDBLevel_SearchTemplates.ObjectRels
        End Get
    End Property

    Public ReadOnly Property OL_Partner As List(Of clsObjectRel)
        Get
            Return objDBLevel_Partners.ObjectRels
        End Get
    End Property

    Public ReadOnly Property OL_Currency As List(Of clsObjectRel)
        Get
            Return objDBLevel_Currencies.ObjectRels
        End Get
    End Property

    Public ReadOnly Property OL_Currencies_Combo As List(Of clsOntologyItem)
        Get
            objDBLevel_Currencies_Combo.Objects1.Sort(Function(LS1 As clsOntologyItem, LS2 As clsOntologyItem) LS1.Name.CompareTo(LS2.Name))
            Return objDBLevel_Currencies_Combo.Objects1
        End Get
    End Property

    Public ReadOnly Property OL_TaxRate As List(Of clsObjectRel)
        Get
            Return objDBLevel_TaxRate.ObjectRels
        End Get
    End Property

    Public ReadOnly Property OL_TaxRates_Combo As List(Of clsOntologyItem)
        Get
            objDBLevel_TaxRate_Combo.Objects1.Sort(Function(LS1 As clsOntologyItem, LS2 As clsOntologyItem) LS1.Name.CompareTo(LS2.Name))
            Return objDBLevel_TaxRate_Combo.Objects1
        End Get
    End Property

    Public ReadOnly Property OL_Unit As List(Of clsObjectRel)
        Get
            Return objDBLevel_Unit.ObjectRels
        End Get
    End Property
    Public ReadOnly Property OL_Units_Combo As List(Of clsOntologyItem)
        Get
            objDBLevel_Unit_Combo.Objects1.Sort(Function(LS1 As clsOntologyItem, LS2 As clsOntologyItem) LS1.Name.CompareTo(LS2.Name))
            Return objDBLevel_Unit_Combo.Objects1
        End Get
    End Property

    Public ReadOnly Property OL_AmountUnit As List(Of clsObjectRel)
        Get
            Return objDBLevel_Unit_Amount_Standard.ObjectRels
        End Get
    End Property

    Public Function get_Data_BaseConfig() As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim objOLGross As New List(Of clsObjectAtt)
        Dim objOLCurrency As New List(Of clsObjectRel)
        Dim objOLLanguages As New List(Of clsObjectRel)
        Dim objOLPartner As New List(Of clsObjectRel)
        Dim objOLTaxRate As New List(Of clsObjectRel)
        Dim objOLPercent As New List(Of clsObjectAtt)
        Dim objOLUnit As New List(Of clsObjectRel)
        Dim objOLUnit_Amount As New List(Of clsObjectRel)
        Dim objOLSearchTemplates As New List(Of clsObjectRel)
        Dim objOLCurrency_Combo As New List(Of clsOntologyItem)
        Dim objOLTaxRate_Combo As New List(Of clsOntologyItem)
        Dim objOLUnit_Combo As New List(Of clsOntologyItem)

        objOLGross.Add(New clsObjectAtt With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                              .ID_AttributeType = objLocalConfig.OItem_Attribute_Gross__Standard_.GUID})

        objDBLevel_Gross_Std.GetDataObjectAtt(objOLGross, _
                                                doIds:=False)

        If objDBLevel_Gross_Std.ObjAtts.Count > 0 Then
            objOLCurrency.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                     .ID_Parent_Other = objLocalConfig.OItem_Class_Currencies.GUID,
                                                     .ID_RelationType = objLocalConfig.OItem_RelationType_Standard.GUID})

            objDBLevel_Currencies.GetDataObjectRel(objOLCurrency, _
                                                     doIds:=False)
            If objDBLevel_Currencies.ObjectRels.Count > 0 Then
                objOLLanguages.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                          .ID_Parent_Other = objLocalConfig.OItem_Class_Language.GUID,
                                                          .ID_RelationType = objLocalConfig.OItem_RelationType_isDescribedBy.GUID})

                objDBLevel_Languages.GetDataObjectRel(objOLLanguages, _
                                                        doIds:=False)

                If objDBLevel_Languages.ObjectRels.Count > 0 Then
                    objOLPartner.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                            .ID_Parent_Other = objLocalConfig.OItem_Class_Partner.GUID,
                                                            .ID_RelationType = objLocalConfig.OItem_RelationType_zugeh_rige_Mandanten.GUID})

                    objDBLevel_Partners.GetDataObjectRel(objOLPartner, _
                                                           doIds:=False)
                    If objDBLevel_Partners.ObjectRels.Count > 0 Then
                        objOLTaxRate.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                                .ID_Parent_Other = objLocalConfig.OItem_Class_Tax_Rates.GUID,
                                                                .ID_RelationType = objLocalConfig.OItem_RelationType_Standard.GUID})
                            
                        objDBLevel_TaxRate.GetDataObjectRel(objOLTaxRate, _
                                                              doIds:=False)

                        If objDBLevel_TaxRate.ObjectRels.Count > 0 Then
                            objOLPercent.Add(New clsObjectAtt With {.ID_Object = objDBLevel_TaxRate.ObjectRels(0).ID_Other,
                                                                    .ID_Class = objLocalConfig.OItem_Class_Tax_Rates.GUID,
                                                                    .ID_AttributeType = objLocalConfig.OItem_Attribute_percent.GUID})

                            objDBLevel_Percent.GetDataObjectAtt(objOLPercent, _
                                                                  doIds:=False)

                            If objDBLevel_Percent.ObjAtts.Count > 0 Then
                                objOLUnit.Add(New clsObjectRel With {.ID_Object = objDBLevel_TaxRate.ObjectRels(0).ID_Other,
                                                                     .ID_Parent_Other = objLocalConfig.OItem_Class_Einheit.GUID,
                                                                     .ID_RelationType = objLocalConfig.OItem_RelationType_is_of_Type.GUID})

                                objDBLevel_Unit.GetDataObjectRel(objOLUnit, _
                                                                   doIds:=False)

                                If objDBLevel_Unit.ObjectRels.Count > 0 Then

                                    objOLSearchTemplates.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_Module.GUID,
                                                                                    .ID_Parent_Other = objLocalConfig.OItem_Class_Search_Template.GUID,
                                                                                    .ID_RelationType = objLocalConfig.OItem_RelationType_offers.GUID})
                                        
                                    objDBLevel_SearchTemplates.GetDataObjectRel(objOLSearchTemplates, _
                                                                                  doIds:=False)

                                    If objDBLevel_SearchTemplates.ObjectRels.Count > 0 Then
                                        objOLCurrency_Combo.Add(New clsOntologyItem With {.GUID_Parent = objLocalConfig.OItem_Class_Currencies.GUID})
                                            
                                        objDBLevel_Currencies_Combo.GetDataObjects(objOLCurrency_Combo)
                                        If objDBLevel_Currencies_Combo.Objects1.Count > 0 Then
                                            objOLTaxRate_Combo.Add(New clsOntologyItem With {.GUID_Parent = objLocalConfig.OItem_Class_Tax_Rates.GUID})
                                                
                                            objDBLevel_TaxRate_Combo.GetDataObjects(objOLTaxRate_Combo)

                                            If objDBLevel_TaxRate_Combo.Objects1.Count > 0 Then
                                                objOLUnit_Combo.Add(New clsOntologyItem With {.GUID_Parent = objLocalConfig.OItem_Class_Einheit.GUID})

                                                objDBLevel_Unit_Combo.GetDataObjects(objOLUnit_Combo)

                                                If objDBLevel_Unit_Combo.Objects1.Count > 0 Then
                                                    objOLUnit_Amount.Add(New clsObjectRel With {.ID_Object = objLocalConfig.OItem_BaseConfig.GUID,
                                                                                                .ID_Parent_Other = objLocalConfig.OItem_Class_Einheit.GUID,
                                                                                                .ID_RelationType = objLocalConfig.OItem_RelationType_Standard.GUID})

                                                    objDBLevel_Unit_Amount_Standard.GetDataObjectRel(objOLUnit_Amount, _
                                                                                                       doIds:=False)
                                                    If objDBLevel_Unit_Amount_Standard.ObjectRels.Count > 0 Then
                                                        objOItem_Result = objLocalConfig.Globals.LState_Success
                                                    Else
                                                        objOItem_Result = objLocalConfig.Globals.LState_Error
                                                    End If

                                                Else
                                                    objOItem_Result = objLocalConfig.Globals.LState_Error
                                                End If

                                            Else
                                                objOItem_Result = objLocalConfig.Globals.LState_Error
                                            End If

                                        Else
                                            objOItem_Result = objLocalConfig.Globals.LState_Error
                                        End If


                                    Else
                                        objOItem_Result = objLocalConfig.Globals.LState_Error
                                    End If
                                Else
                                    objOItem_Result = objLocalConfig.Globals.LState_Error
                                End If
                            Else
                                objOItem_Result = objLocalConfig.Globals.LState_Error
                            End If
                        Else
                            objOItem_Result = objLocalConfig.Globals.LState_Error
                        End If
                    Else
                        objOItem_Result = objLocalConfig.Globals.LState_Error
                    End If
                Else
                    objOItem_Result = objLocalConfig.Globals.LState_Error
                End If
            Else
                objOItem_Result = objLocalConfig.Globals.LState_Error
            End If
        Else
            objOItem_Result = objLocalConfig.Globals.LState_Error
        End If

        Return objOItem_Result
    End Function

    Public Sub New(ByVal LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig
        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_Gross_Std = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Currencies = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Currencies_Combo = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Languages = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Partners = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_TaxRate = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_TaxRate_Combo = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Percent = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Unit = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Unit_Combo = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Unit_Amount_Standard = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_SearchTemplates = New OntologyModDBConnector(objLocalConfig.Globals)
    End Sub
End Class

