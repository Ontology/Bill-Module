﻿Imports OntologyAppDBConnector
Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Public Class clsDataWork_Payments
    Private objLocalConfig As clsLocalConfig

    Private objDBLevel_Payments As OntologyModDBConnector
    Private objDBLevel_Payments_Amount As OntologyModDBConnector
    Private objDBLevel_Payments_TransactionDate As OntologyModDBConnector
    Private objDBLevel_Payments_Part As OntologyModDBConnector
    Private objDBLevel_Payments_Transaction_Sparkasse As OntologyModDBConnector
    Private objDBLevel_BankTransaction_GegenKonto As OntologyModDBConnector
    Private objDBLevel_GegenKonto_Bank As OntologyModDBConnector
    Private objDBLevel_BankTransaction_Valutadatum As OntologyModDBConnector
    Private objDBLevel_BankTransaction_BegZahl As OntologyModDBConnector
    Private objDBLevel_BankTransaction_Betrag As OntologyModDBConnector

    Private objOItem_Result_Payments As clsOntologyItem

    Private objThread_Payments As Threading.Thread

    Private dtblT_Payments As New DataSet_BillModule.dtbl_PaymentsDataTable

    Private objOItem_Transaction As clsOntologyItem

    Private dblSum As Double
    Private dblSumPart As Double

    Public ReadOnly Property Payments_Sum As Double
        Get
            Return dblSum
        End Get
    End Property

    Public ReadOnly Property Payments_SumPart As Double
        Get
            Return dblSumPart
        End Get
    End Property

    Public ReadOnly Property DataTable_Payment As DataSet_BillModule.dtbl_PaymentsDataTable
        Get
            Return dtblT_Payments
        End Get
    End Property

    Public ReadOnly Property OItem_Result_Payment As clsOntologyItem
        Get
            Return objOItem_Result_Payments
        End Get
    End Property


    Public Sub get_Data_Payments(ByVal OItem_Transaction As clsOntologyItem)
        objOItem_Transaction = OItem_Transaction

        dblSum = 0
        objOItem_Result_Payments = objLocalConfig.Globals.LState_Nothing

        dtblT_Payments.Clear()

        Try
            objThread_Payments.Abort()
        Catch ex As Exception

        End Try

        objThread_Payments = New Threading.Thread(AddressOf get_Data_Payments_Thread)
        objThread_Payments.Start()


    End Sub


    Private Sub get_Data_Payments_Thread()
        Dim objOLTransaction_To_Payments As New List(Of clsObjectRel)
        Dim objOLPayment__Amount As New List(Of clsObjectAtt)
        Dim objOLPayment__TransactionDate As New List(Of clsObjectAtt)
        Dim objOLPayment__Part As New List(Of clsObjectAtt)
        Dim objOLBankTransaction_To_Payment As New List(Of clsObjectRel)
        Dim objOLBankTransaction__Valutadatum As New List(Of clsObjectAtt)
        Dim objOLBankTransaction__BegZahl As New List(Of clsObjectAtt)
        Dim objOLBankTransaction__Betrag As New List(Of clsObjectAtt)
        Dim objOLBankTransaction_To_Konto As New List(Of clsObjectRel)
        Dim objOLBankKonto_To_Bank As New List(Of clsObjectRel)

        objOLTransaction_To_Payments.Add(New clsObjectRel With {.ID_Object = objOItem_Transaction.GUID,
                                                                .ID_Parent_Other = objLocalConfig.OItem_Class_Payment.GUID,
                                                                .ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Payment.GUID})

        objDBLevel_Payments.GetDataObjectRel(objOLTransaction_To_Payments, _
                                               doIds:=False)

        objOLPayment__Amount.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Payment.GUID,
                                                        .ID_AttributeType = objLocalConfig.OItem_Attribute_Amount.GUID})


        objDBLevel_Payments_Amount.GetDataObjectAtt(objOLPayment__Amount, _
                                                      doIds:=False)

        objOLPayment__TransactionDate.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Payment.GUID,
                                                                 .ID_AttributeType = objLocalConfig.OItem_Attribute_Transaction_Date.GUID})

        objDBLevel_Payments_TransactionDate.GetDataObjectAtt(objOLPayment__TransactionDate, _
                                                               doIds:=False)

        objOLPayment__Part.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Payment.GUID,
                                                      .ID_AttributeType = objLocalConfig.OItem_Attribute_part____.GUID})

        objDBLevel_Payments_Part.GetDataObjectAtt(objOLPayment__Part, _
                                                    doIds:=False)


        objOLBankTransaction_To_Payment.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Class_Bank_Transaktionen__Sparkasse_.GUID,
                                                                   .ID_Parent_Other = objLocalConfig.OItem_Class_Payment.GUID,
                                                                   .ID_RelationType = objLocalConfig.OItem_RelationType_belongsTo.GUID})

        objDBLevel_Payments_Transaction_Sparkasse.GetDataObjectRel(objOLBankTransaction_To_Payment, _
                                                                     doIds:=False)


        objOLBankTransaction__Valutadatum.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Bank_Transaktionen__Sparkasse_.GUID,
                                                                     .ID_AttributeType = objLocalConfig.OItem_Attribute_Valutatag.GUID})

        objDBLevel_BankTransaction_Valutadatum.GetDataObjectAtt(objOLBankTransaction__Valutadatum, _
                                                           doIds:=False)


        objOLBankTransaction__BegZahl.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Bank_Transaktionen__Sparkasse_.GUID,
                                                                 .ID_AttributeType = objLocalConfig.OItem_Attribute_Beg_nstigter_Zahlungspflichtiger.GUID})
            
        objDBLevel_BankTransaction_BegZahl.GetDataObjectAtt(objOLBankTransaction__BegZahl, _
                                                       doIds:=False)

        objOLBankTransaction__Betrag.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Class_Bank_Transaktionen__Sparkasse_.GUID,
                                                                .ID_AttributeType = objLocalConfig.OItem_Attribute_Betrag.GUID})

        objDBLevel_BankTransaction_Betrag.GetDataObjectAtt(objOLBankTransaction__Betrag, _
                                                             doIds:=False)

        objOLBankTransaction_To_Konto.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Class_Bank_Transaktionen__Sparkasse_.GUID,
                                                                 .ID_Parent_Other = objLocalConfig.OItem_Class_Kontonummer.GUID,
                                                                 .ID_RelationType = objLocalConfig.OItem_RelationType_Gegenkonto.GUID})

        objDBLevel_BankTransaction_GegenKonto.GetDataObjectRel(objOLBankTransaction_To_Konto, _
                                                                 doIds:=False)

        objOLBankKonto_To_Bank.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Class_Kontonummer.GUID,
                                                          .ID_Parent_Other = objLocalConfig.OItem_Class_Bankleitzahl.GUID,
                                                          .ID_RelationType = objLocalConfig.OItem_RelationType_belongsTo.GUID})

        objDBLevel_GegenKonto_Bank.GetDataObjectRel(objOLBankKonto_To_Bank, _
                                                      doIds:=False)


        Dim objLPaymentPre = From objPayment In objDBLevel_Payments.ObjectRels
                          Join objAmount In objDBLevel_Payments_Amount.ObjAtts On objPayment.ID_Other Equals objAmount.ID_Object
                          Join objTransactionDate In objDBLevel_Payments_TransactionDate.ObjAtts On objPayment.ID_Other Equals objTransactionDate.ID_Object
                          Join objPart In objDBLevel_Payments_Part.ObjAtts On objPayment.ID_Other Equals objPart.ID_Object
                          Select ID_Transaction = objPayment.ID_Object, _
                                 ID_Payment = objPayment.ID_Other, _
                                 Name_Payment = objPayment.Name_Other, _
                                 ID_Attribute_Amount = objAmount.ID_Attribute, _
                                 Val_Amount = objAmount.Val_Double, _
                                 ID_Attribute_TransactionDate = objTransactionDate.ID_Attribute, _
                                 Val_TransactionDate = objTransactionDate.Val_Date, _
                                 ID_Attribute_Part = objPart.ID_Attribute, _
                                 Val_Part = objPart.Val_Double

        Dim objLBankKonto = From objBankKonto In objDBLevel_Payments_Transaction_Sparkasse.ObjectRels
                            Join objValuta In objDBLevel_BankTransaction_Valutadatum.ObjAtts On objValuta.ID_Object Equals objBankKonto.ID_Object
                            Join objBegZah In objDBLevel_BankTransaction_BegZahl.ObjAtts On objBegZah.ID_Object Equals objBankKonto.ID_Object
                            Join objBetrag In objDBLevel_BankTransaction_Betrag.ObjAtts On objBetrag.ID_Object Equals objBankKonto.ID_Object
                            Join objGegenKonto In objDBLevel_BankTransaction_GegenKonto.ObjectRels On objGegenKonto.ID_Object Equals objBankKonto.ID_Object
                            Join objGegenBank In objDBLevel_GegenKonto_Bank.ObjectRels On objGegenBank.ID_Object Equals objGegenKonto.ID_Other
                            Select ID_Payment = objBankKonto.ID_Other, _
                                   ID_BankTransaction = objBankKonto.ID_Object, _
                                   Name_BankTransaction = objBankKonto.Name_Object, _
                                   ID_Attribute_Valutadatum = objValuta.ID_Attribute, _
                                   Val_Valutadatum = objValuta.Val_Date, _
                                   ID_Attribute_BegZahl = objBegZah.ID_Attribute, _
                                   Val_BegZahl = objBegZah.Val_String, _
                                   ID_Attribute_Betrag = objBetrag.ID_Attribute, _
                                   Val_Betrag = objBetrag.Val_Double, _
                                   ID_Gegenkonto = objGegenKonto.ID_Other, _
                                   Name_Gegenkonto = objGegenKonto.Name_Other, _
                                   ID_Bank = objGegenBank.ID_Other, _
                                   Name_Bank = objGegenBank.Name_Other




        dblSum = (From obj In objLPaymentPre
                  Select obj.Val_Amount).Sum()

        dblSumPart = (From obj In objLPaymentPre
                      Select obj.Val_Amount / 100 * obj.Val_Part).Sum()

        Dim objLPayment = From objPayment In objLPaymentPre
                          Group Join objBankKonto In objLBankKonto On objPayment.ID_Payment Equals objBankKonto.ID_Payment Into objBankKontos = Group
                          From objBankKonto In objBankKontos.DefaultIfEmpty

        For Each objPayment In objLPayment
            If objPayment.objBankKonto Is Nothing Then
                dtblT_Payments.Rows.Add(objPayment.objPayment.ID_Transaction, _
                                    objPayment.objPayment.ID_Payment, _
                                    objPayment.objPayment.Name_Payment, _
                                    objPayment.objPayment.ID_Attribute_Amount, _
                                    objPayment.objPayment.Val_Amount, _
                                    objPayment.objPayment.ID_Attribute_TransactionDate, _
                                    objPayment.objPayment.Val_TransactionDate, _
                                    objPayment.objPayment.ID_Attribute_Part, _
                                    objPayment.objPayment.Val_Part)
            Else
                dtblT_Payments.Rows.Add(objPayment.objPayment.ID_Transaction, _
                                    objPayment.objPayment.ID_Payment, _
                                    objPayment.objPayment.Name_Payment, _
                                    objPayment.objPayment.ID_Attribute_Amount, _
                                    objPayment.objPayment.Val_Amount, _
                                    objPayment.objPayment.ID_Attribute_TransactionDate, _
                                    objPayment.objPayment.Val_TransactionDate, _
                                    objPayment.objPayment.ID_Attribute_Part, _
                                    objPayment.objPayment.Val_Part, _
                                    objPayment.objBankKonto.ID_BankTransaction, _
                                    objPayment.objBankKonto.Name_BankTransaction, _
                                    objPayment.objBankKonto.ID_Attribute_Valutadatum, _
                                    objPayment.objBankKonto.Val_Valutadatum, _
                                    objPayment.objBankKonto.ID_Attribute_BegZahl, _
                                    objPayment.objBankKonto.Val_BegZahl, _
                                    objPayment.objBankKonto.ID_Attribute_Betrag, _
                                    objPayment.objBankKonto.Val_Betrag, _
                                    objPayment.objBankKonto.ID_Gegenkonto, _
                                    objPayment.objBankKonto.Name_Gegenkonto, _
                                    objPayment.objBankKonto.ID_Bank, _
                                    objPayment.objBankKonto.Name_Bank)
            End If


        Next

        objOItem_Result_Payments = objLocalConfig.Globals.LState_Success
    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig
        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_Payments = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Payments_Amount = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Payments_TransactionDate = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Payments_Part = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Payments_Transaction_Sparkasse = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BankTransaction_GegenKonto = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_GegenKonto_Bank = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BankTransaction_Valutadatum = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BankTransaction_BegZahl = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BankTransaction_Betrag = New OntologyModDBConnector(objLocalConfig.Globals)
    End Sub
End Class
